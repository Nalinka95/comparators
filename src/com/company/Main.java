package com.company;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

public class Main {


    public static void main(String[] args) {
	// write your code here
        Main m1= new Main();

        LinkedList<Movie> movieList= new LinkedList<Movie>();
        movieList.add(new Movie("Matrix",8.9,1995));
        movieList.add(new Movie("Lord of the ring",9.2,2005));
        movieList.add(new Movie("Harry potter",9.5,2001));
        movieList.add(new Movie("Pk",8.3,2014));
        movieList.add(new Movie("Gajini",9.3,2002));
        System.out.println("... unsorted.................");
        m1.printList(movieList);

        Collections.sort(movieList,new Sortbyname());

        System.out.println("... sorted by name.................");
        m1.printList(movieList);
        Collections.sort(movieList,new SortbyRating());

        System.out.println("... sorted. by Rating................");
        m1.printList(movieList);

        Collections.sort(movieList,new SortbyReleaseYear());

        System.out.println("... sorted by year.................");
        m1.printList(movieList);
    }
    private void printList(LinkedList<Movie> list){
        for(Movie movie: list){
            System.out.println("name : "+movie.getName()+" Rating  : "+movie.getRating()+" Release Year"+movie.getReleaseyear());
        }
    }
}
class SortbyReleaseYear implements Comparator<Movie>
{

    public int compare(Movie a, Movie b)
    {
        return a.getReleaseyear() - b.getReleaseyear();
    }
}
class SortbyRating implements Comparator<Movie>
{

    public int compare(Movie a, Movie b)
    {
        double subtraction=a.getRating() - b.getRating();
        if(subtraction==0){
            return 0;
        }
        else if(subtraction>0){
            return 1;
        }
        else{
            return -1;
        }
    }
}

class Sortbyname implements Comparator<Movie>
{

    public int compare(Movie a, Movie b)
    {
        return a.getName().compareTo(b.getName());
    }
}
class Movie{
    private String name;
    private Double rating;
    private Integer releaseyear;

    public Movie(String name, Double rating, Integer releaseyear) {
        this.name = name;
        this.rating = rating;
        this.releaseyear = releaseyear;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getReleaseyear() {
        return releaseyear;
    }

    public void setReleaseyear(Integer releaseyear) {
        this.releaseyear = releaseyear;
    }
}
